Process within 3 hours:

0. Read challange task carefully and get resources
1. Download and install Java JDK and Netbeans
2. Open Git Project and connect with Souretree and push initial commit
3. Create Java-Project and commit
4. Development
    4.1 Try to get .xls with JExcelApi
        >> doesnt work because of .csv
    4.2 Research about import .csv into java
        >> Solution with simple list 
    4.3 Convert String Array (Column min and max) into Int with Integer.parseInt
    4.4 Substract max with min and write it in var
    4.5 Try to get min with For-Loop
        >> Unfortunately it doesnt work (time over)
5. Final commit
    